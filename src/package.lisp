(defpackage :sdds
  (:use :common-lisp :asdf :os)
  (:export
    ;; directory.lisp
    #:create-user-directory
    #:update-user-directory
    #:delete-user-directory
    #:check-user-directory
    #:user-directory-permission))

(defsystem "sdds"
  :name "sdds"
  :version "0.0.1"
  :maintainer "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :author "FOSSilized_Daemon (fossilizeddaemon@protonmail.com)"
  :license "AGPL (see LISENCE for details)."
  :description "A common-lisp library for dotfile deployment."
  :long-description "A common-lisp library for dotfile deployment."
  :depends-on ("os")
  :components ((:file "package")
               (:file "directory")))

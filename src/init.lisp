;; Set the home directory.
(defvar *home-directory* (user-homedir-pathname))

;; Set the configuration file directory.
(defvar *configuration-directory* (merge-pathnames ".config/" *home-directory*))

;; Set the data file directory.
(defvar *data-directory* (merge-pathnames ".usr/share/" *home-directory*))

;; Set the cache file directory.
(defvar *cache-directory* (merge-pathnames ".var/cache/" *home-directory*))

;; Set the state file directory.
(defvar *state-directory* (merge-pathnames ".var/state/" *home-directory*))

;; Set the log file directory.
(defvar *log-directory* (merge-pathnames ".var/log/" *home-directory*))

;; Set the run file directory.
(defvar *run-directory* (merge-pathnames ".var/run/" *home-directory*))

;; Set the library directory.
(defvar *library-directory* (merge-pathnames ".library/" *home-directory*))

;; Set the program directory.
(defvar *program-directory* (merge-pathnames ".program/" *home-directory*))

;; Set the download directory.
(defvar *download-directory* (merge-pathnames "transfer/download/" *home-directory*))

;; Set the upload directory.
(defvar *upload-directory* (merge-pathnames "transfer/upload/" *home-directory*))
